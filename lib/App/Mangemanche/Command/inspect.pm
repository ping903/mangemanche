package App::Mangemanche::Command::inspect;
use strict;
use warnings;
use parent 'App::Mangemanche::Command';
use App::Mangemanche::Command ':exit_codes';

sub iplist_dumper {
    my ($self, $kw, $val) = @_;
    print "$kw <<EOF\n";
    foreach my $ip (@$val) {
	print "    $ip\n";
    }
    print "EOF\n";
}

sub auth_dumper {
    my ($self, $kw, $val) = @_;
    foreach my $s (@$val) {
	print "$kw $s->{type} $s->{method}";
	if ($s->{'passwd-file'}) {
	    print " $s->{'passwd-file'} $s->{realm}";
	}
	print "\n";		
    }
}

my %kwdump = (
    'ip-list' => \&iplist_dumper,
    'trusted-ip' => \&iplist_dumper,
    'auth' => \&auth_dumper
    );

sub run {
    my $self = shift;
    $self->SUPER::run;
    $self->usage_error("extra parameters") if @ARGV > 1;

    my $cfg = $self->agent->get_config(@ARGV);
    unless ($cfg) {
	$self->abend(EX_FAIL, $self->agent->error_message);
    }

    foreach my $kw (sort keys %$cfg) {
	my $val = $cfg->{$kw} or next;
	if (my $d = $kwdump{$kw}) {
	    $self->${ \$d }($kw, $val);
	} elsif (ref($val) eq 'ARRAY') {
	    print "# $kw " . JSON::encode_json($val) . "\n";
	} elsif (JSON::is_bool($val)) {
	    print "$kw ";
	    print $val ? "on" : "off";
	    print "\n";
	} else {
	    print "$kw $val\n";
	}
    }
}

1;

=head1 NAME

mangemanche inspect - inspect ping903 server configuration

=head1 SYNOPSIS

B<inspect>
[I<PARAM>]

=head1 DESCRIPTION

Queries the running instance of B<ping903> about its configuration and
prints it on screen in its configuration file format.

If given a single argument I<PARAM>, only the requested configuration
parameter is displayed.

=head1 OPTIONS

=head2 Informative options

=over 4

=item B<-?>

Display short help summary.

=item B<--usage>

Display command line usage summary.

=item B<--help>

Display a detailed program manual.

=back

=head1 NOTES

Depending on the B<ping903> server and the user settings, the configuration
returned can be censored.

The IP list is always formatted as a here-document, containing both immutable
and mutable IPs from the server configuration.

=head1 SEE ALSO

L<ping903>(1), L<ping903.conf>(5).
