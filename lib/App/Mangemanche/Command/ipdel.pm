package App::Mangemanche::Command::ipdel;
use strict;
use warnings;
use parent 'App::Mangemanche::Command';
use App::Mangemanche::Command ':exit_codes';

sub run {
    my $self = shift;
    $self->SUPER::run;
    $self->usage_error("required parameters missing") unless @ARGV == 1;
    my $ip = shift @ARGV;
    unless ($self->agent->ipdel($ip)) {
	$self->abend(EX_FAIL, $self->agent->error_message);
    }
}
1;
=head1 NAME

ipdel - deletes a single IP from the ping903 mutable ip list

=head1 SYNOPSIS

B<mangemanche ipdel> I<IP>

=head1 DESCRIPTION

Deletes I<IP> from the mutable IP list of the running B<ping903> daemon.

=head1 OPTIONS

=head2 Informative options

=over 4

=item B<-?>

Display short help summary.

=item B<--usage>

Display command line usage summary.

=item B<--help>

Display a detailed program manual.

=back

=head1 SEE ALSO

L<ping903>,
L<mangemache>,
L<ipadd>.

=cut
