package App::Mangemanche::Command::ident;
use strict;
use warnings;
use parent 'App::Mangemanche::Command';
use App::Mangemanche::Command ':exit_codes';

sub run {
    my $self = shift;
    $self->SUPER::run;
    $self->usage_error("extra parameters") if @ARGV > 1;

    my $id = $self->agent->get_id(@ARGV);
    unless ($id) {
	$self->abend(EX_FAIL, $self->agent->error_message);
    }

    foreach my $kw (sort keys %$id) {
	my $val = $id->{$kw} or next;
	print "$kw ";
	if (ref($val) eq 'ARRAY') {
	    print join(' ', @$val);
	} elsif (JSON::is_bool($val)) {
	    print $val ? "on" : "off";
	} else {
	    print $val
	}
	print "\n";
    }
}

1;

=head1 NAME

ident - display ping903 server identification

=head1 SYNOPSIS

B<mangemanche ident>
[I<PARAM>]

=head1 DESCRIPTION

Queries the running instance of B<ping903> about identification parameters
and prints them on screen.  The identification parameters are: package name,
package version an running instance PID.

If given a single argument I<PARAM>, only the requested parameter is
displayed.

=head1 OPTIONS

=head2 Informative options

=over 4

=item B<-?>

Display short help summary.

=item B<--usage>

Display command line usage summary.

=item B<--help>

Display a detailed program manual.

=back

=head1 SEE ALSO

L<ping903>(1), L<ping903.conf>(5).
