package App::Mangemanche::Command::dbload;
use strict;
use warnings;
use parent 'App::Mangemanche::Command';
use App::Mangemanche::Command ':exit_codes';
use JSON;
use DBI;
use File::Spec;

sub new {
    my ($class, $com, $agent) = @_;

    my $self = $class->SUPER::new($com, $agent,
	    'D|driver=s' => 'driver',
	    'd|database=s' => sub {
		my $self = shift;
		push @{$self->{options}{connarg}}, "database=$_[1]";
	    },
	    'h|host=s' => sub {
		my $self = shift;
		push @{$self->{options}{connarg}}, "host=$_[1]";
	    },
	    'P|port=s' => sub {
		my $self = shift;
		push @{$self->{options}{connarg}}, "port=$_[1]";
	    },
	    'u|user=s' => 'dbuser',
	    'p|password=s' => 'dbpass',
	    'params=s' => 'dbparams',
	    'defaults-file=s' => 'defaults-file',
	    't|table=s' => 'table',
	    'c|column=s' => 'column',
	    'q|query=s' => 'query');

    $self->{options}{driver} = 'mysql';

    $self;
}

sub run {
    my $self = shift;
    $self->SUPER::run;
    $self->usage_error("extra parameters") if @ARGV;

    my $query = $self->option('query');
    unless ($query) {
	$self->usage_error("--table option must be specified")
	    unless $self->option('table');
	$self->usage_error("--column option must be specified")
	    unless $self->option('column');
	$query = qq{SELECT $self->{options}{column} FROM $self->{options}{table}};
    }

    if (my $p = $self->option('dbparams')) {
	push @{$self->{options}{connarg}}, $p;
    }

    unless ($self->{options}{connarg}) {
	$self->usage_error('Database parameters not initialized.  Please use the --database (optionally - --host and --port) option.');
    }

    if ($self->option('driver') eq 'mysql') {
	unless ($self->option('defaults-file')) {
	    my $f = File::Spec->catfile($ENV{HOME}, '.my.cnf');
	    if (-f $f) {
		$self->option('defaults-file', $f);
	    }
	}
    }
    if (my $p = $self->option('defaults-file')) {
	push @{$self->{options}{connarg}}, ";mysql_read_default_file=$p";
    }

    my $arg = join(':', ('DBI',$self->{options}{driver},
			 @{$self->{options}{connarg}}));

    my $dbh = DBI->connect($arg, $self->{options}{dbuser},
			   $self->{options}{dbpass},
			   { RaiseError => 0, PrintError => 1, AutoCommit => 1})
	or $self->abend(EX_FAIL, "can't connect to the database server");

    my $res = $dbh->selectall_arrayref($query,
				       { RaiseError => 0, PrintError => 1 })
	or $self->abend(EX_FAIL, "query failed");

    unless ($self->agent->set_ip_list([ map { $_->[0] } @$res ])) {
	$self->abend(EX_FAIL, $self->agent->error_message);
    }
}

1;

=head1 NAME

dbload - Load IP addresses from database into Ping903

=head1 SYNOPSIS

B<mangemanche dbload>
[B<-D> I<DRIVER>]
[B<-P> I<DBPORT>]
[B<-c> I<COLUMN>]
[B<-d> I<DBNAME>]
[B<-h> I<DBHOST>]
[B<-p> I<DBPASS>]
[B<-q> I<QUERY>]
[B<-t> I<TABLE>]
[B<-u> I<DBUSER>]
[B<--column>=I<COLUMN>]
[B<--database>=I<DBNAME>]
[B<--defaults-file>=I<FILE>]
[B<--driver>=I<DRIVER>]
[B<--host>=I<DBHOST>]
[B<--params>=I<DBPARAM>]
[B<--password>=I<DBPASS>]
[B<--port>=I<DBPORT>]
[B<--query>=I<QUERY>]
[B<--table>=I<TABLE>]
[B<--user>=I<DBUSER>]

=head1 DESCRIPTION

Reads IP addresses from the database and adds them to the list of monitored
hosts of L<ping903>.

Most parameters are configurable.  You need to supply at least the database
connection information and the query to use.  For the latter, use the
B<--table> and B<--column> options together.  For really complex queries,
use the B<--query> option instead.  See below for details.

On success, prints on standard output the number of IP addresses loaded and
exits with status 0.  On error, displays on stabdard error the detailed
diagnostic information as obtained from the server and exits with status 1.

=head1 OPTIONS

=head2 Database connection options

=over 4

=item B<-D>, B<--driver>=I<DRIVER>

Sets L<DBI> database driver name.  Default is C<mysql>.

=item B<-h>, B<--host>=I<NAME>

Name or IP address of the database server.

=item B<-P>, B<--port>=I<PORT>

Port number the database server listens on.

=item B<-d>, B<--database>=I<NAME>

Name of the database to use.

=item B<-u>, B<--user>=I<USER>

Database user name.

=item B<-p>, B<--password>=I<PASS>

Database password.

=item B<--defaults-file>=I<FILE>

Name of the MySQL defaults file to use.  This option is used only if DBI
driver B<mysql> is used.  By default, the file F<.my.cnf> in the home
directory is used, if it exists.

=item B<--params>=I<STRING>

Additional parameters for the DBI driver.  For detailed information, refer
to the documentation of the driver in use.

=back

=head2 Query to extract IP addresses from the database

Three options are provided:

=over 4

=item B<-t>, B<--table>=I<TABLE>

Name of the table which holds IP addresses.

=item B<-c>, B<--column>=I<NAME>

Name of the column in I<TABLE> where IP address is stored.  If need
be, you can use SQL expression as well.

=item B<-q>, B<--query>=I<STRING>

SQL query to use in order to obtain IP addresses.  This overrides the two
options above.

=back

Normally you would use a combination of B<--table> and B<--column> options.
For example, if you have a database table C<hosts>, which has a column C<ip>,
that holds IP address in dotted-quad form, you would use the following options:

    --table=hosts --column=ip

If the table holds IP addresses in numeric form, use the following instead
(MySQL is assumed):

    --table=hosts --column='INET_NTOA(ip)'

Finally, if a complex query is needed to extract IP addresses (e.g. joining
several tables, etc.), supply it in full, via the B<--query> option.

=head2 Informative options

=over 4

=item B<-?>

Display short help summary.

=item B<--usage>

Display command line usage summary.

=item B<--help>

Display a detailed program manual.

=back

=head1 SEE ALSO

L<ping903>,
L<DBI>,
L<mangemache>.

=cut
