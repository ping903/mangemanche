package App::Mangemanche::Command::nagios;
use strict;
use warnings;
use parent 'App::Mangemanche::Command';
use App::Mangemanche::Command ':exit_codes';
use File::Spec;
use File::Basename;
use Nagios::Config;

sub new {
    my ($class, $com, $agent) = @_;

    $class->SUPER::new($com, $agent,
		       'progname|p=s@' => 'prognames',
		       'config|c=s' => 'nagiosconf');
}

sub valid_host {
    my ($self,$s) = @_;
    return 1 if $s =~ /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}
                        (?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/x;
    unless (gethostbyname($s)) {
	$self->error("warning: $s does not resolve; skipping");
	return 0;
    }
    return 1;
}
	
sub run {
    my $self = shift;
    $self->SUPER::run;
    $self->usage_error("extra parameters") if @ARGV;
    my $cf = $self->{options}{nagiosconf} // '/etc/nagios/nagios.cfg';
    push @{$self->{options}{prognames}}, 'ping903q';
    $self->{cfg} = Nagios::Config->new(filename => $cf);

    my %iph;
    foreach my $host ($self->{cfg}->list_hosts) {
	if ($host->register && $self->check_command($host)) {
	    $iph{$host->address} = 1;
	}
    }
    foreach my $service ($self->{cfg}->list_services) {
	if ($service->register && $self->check_command($service)) {
	    my @addr = map { $_->address } (@{$service->host_name});
	    @iph{@addr} = (1) x @addr
	}
    }

    my @iplist = sort grep { $self->valid_host($_) } keys %iph;
    my $n = @iplist;
    $self->error("info: collected $n addresses");
    unless ($self->agent->set_ip_list([@iplist])) {
	$self->abend(EX_FAIL, $self->agent->error_message);
    }
}

sub check_command {
    my ($self, $host) = @_;
    my $chk = $host->check_command or return;
    $chk =~ s/!.*//;
    my $def = $self->{cfg}->find_object($chk) or return;
    return unless $def->can('command_line');
    my ($cmd) =  split(/\s+/, $def->command_line);
    return grep {
		File::Spec->file_name_is_absolute($_)
		    ? $cmd eq $_
		    : basename($cmd) eq $_
	    } @{$self->{options}{prognames}};
}

1;
=head1 NAME

nagios - extract monitored IP addresses from the Nagios configuration

=head1 SYNOPSIS

B<mangemanche nagios>
[B<-c> I<FILE>]
[B<-p> I<PROG>]
[B<--config=>I<FILE>]
[B<--progname=>I<PROG>]

=head1 DESCRIPTION

Scans Nagios configuration for services that use B<ping903q> and extracts
IP addresses of associated hosts.  At the end of the run, the extracted
addresses are posted to the mutable list of IP addresses monitored by
the running B<ping903> server.

Default nagios configuration file is F</etc/nagios/nagios.conf>.

=head1 OPTIONS

=over 4

=item B<-c>, B<--config=>I<FILE>

Read Nagios configuration from I<FILE>.

=item B<-p>, B<--progname=>I<PROG>

Look for I<PROG> in addition to B<ping903q>.

=back

=head1 NOTES

If your Nagios configuration makes use of multiple template inheritance,
you would need a modified version of Nagios::Config, available from

L<https://github.com/graygnuorg/perl-nagios-object>.

The bug was reported to the upstream.  See
L<https://rt.cpan.org/Ticket/Display.html?id=132053>.
    
=head1 SEE ALSO

L<ping903>,
L<mangemache>,
L<ipadd>,
L<ipdel>.

=cut
