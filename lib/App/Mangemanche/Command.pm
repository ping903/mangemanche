package App::Mangemanche::Command;
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Pod::Find qw(pod_where);
use Carp;
use parent 'Exporter';
use Pod::Man;
use File::Temp qw(tempfile);

use constant {
    EX_OK => 0,
    EX_FAIL => 1,
    EX_USAGE => 2
};

our @EXPORT_OK = qw(EX_OK EX_FAIL EX_USAGE mandoc);
our %EXPORT_TAGS = ( 'exit_codes' => [
			 qw(EX_OK EX_FAIL EX_USAGE)
		     ] );

sub mandoc {
    my ($class, $name) = @_;

    my $input = pod_where({-inc => 1}, $class);
    unless ($name) {
	($name = $class) =~ s/.*:://;
    }
    my $parser = Pod::Man->new(name => $name,
			       center => 'Ping903 control command',
			       release => 'Mangemanche');
    my ($fh,$filename) = tempfile();
    $parser->parse_from_file($input, $filename);
    close($fh);
    system("man $filename");
    unlink($filename);
    exit(0);
}

sub new {
    my ($class, $com, $agent, %opts) = @_;
    my $self = bless { progname => $com, agent => $agent }, $class;

    $self->{options} = {};

    $self->{optdef}{'shorthelp|?'} = sub {
	pod2usage(-message => $class->pod_usage_msg,
		  -input => pod_where({-inc => 1}, $class),
		  -exitstatus => EX_OK)
    };
    $self->{optdef}{help} = sub {
	mandoc($class);
    };
    $self->{optdef}{usage} = sub {
	pod2usage(-exitstatus => EX_OK,
		  -verbose => 0,
		  -input => pod_where({-inc => 1}, $class))
    };

    foreach my $k (keys %opts) {
	if (ref($opts{$k}) eq 'CODE') {
	    $self->{optdef}{$k} = sub { &{$opts{$k}}($self, @_ ) }
	} elsif (ref($opts{$k})) {
	    $self->{optdef}{$k} = $opts{$k};
	} else {
	    $self->{optdef}{$k} = \$self->{options}{$opts{$k}}
	}
    }
    $self;
}

sub run {
    my $self = shift;
    GetOptions(%{$self->{optdef}}) or exit(EX_USAGE);
}

sub agent { shift->{agent} }

sub option {
    my $self = shift;
    my $name = shift;
    if (defined(my $v = shift)) {
	croak "too many arguments" if @_;
	$self->{options}{$name} = $v;
    }
    return $self->{options}{$name};
}

sub pod_usage_msg {
    my ($class) = @_;

    my $msg = "";

    open my $fd, '>', \$msg;

    my $input = pod_where({-inc => 1}, $class);

    pod2usage(-verbose => 99,
	      -sections => 'NAME',
	      -output => $fd,
	      -exitval => 'NOEXIT',
	      -input => $input);

    my @a = split /\n/, $msg;
    if ($#a < 1) {
	croak "missing or malformed NAME section in " . ($input // $0);
    }
    $msg = $a[1];
    $msg =~ s/^\s+//;
    $msg =~ s/ - /: /;
    return $msg;
}

sub error {
    my ($self, @msg) = @_;
    print STDERR "$self->{progname}: ";
    print STDERR "@msg\n";
}

sub abend {
    my ($self, $code, @msg) = @_;
    $self->error(@msg);
    exit $code;
}

sub usage_error {
    my $self = shift;
    $self->abend(EX_USAGE, @_);
}

1;
